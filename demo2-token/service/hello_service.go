package service

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"strconv"
)

// hello服务具体实现

type HelloService struct{}

// 方法名&参数  --> 就在 hello.pb.go 的 HelloServiceServer
func (server *HelloService) SayHello(ctx context.Context, req *HelloRequest) (*HelloResponse, error) {
	// 身份鉴权
	if !AuthToken(ctx) {
		return nil, grpc.ErrServerStopped
	}
	number := strconv.Itoa(int(req.Id))
	resp := &HelloResponse{Name: "hello word no." + number}
	return resp, nil
}

// token鉴权
func AuthToken(ctx context.Context) bool {
	// FromIncomingContext 用于 `服务端` 获取request中的metadata
	// FromOutgoingContext 用于 `客户端` 获取自己即将发出的metadata
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return false
	}
	if val, ok := md["token"]; ok {
		return val[0] == "real_token"
	}
	// 未找到
	return false
}
