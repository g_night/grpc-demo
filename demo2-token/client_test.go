package main

// 客户端示例

import (
	"context"
	"demo2-token/service"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
	"testing"
)

// customCredential 自定义认证
type customCredential struct{}

// GetRequestMetadata 实现自定义认证接口
func (c customCredential) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		"token": "real_token",
	}, nil
}

// RequireTransportSecurity 自定义认证是否开启TLS
func (c customCredential) RequireTransportSecurity() bool {
	return false
}

func TestClient(t *testing.T) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithPerRPCCredentials(new(customCredential)))
	normalClient(opts)
}

// 常规请求操作
func normalClient(opts []grpc.DialOption) {
	// 连接 127.0.0.1:9091，地址要一致
	// 加入token
	conn, err := grpc.Dial("127.0.0.1:9091", opts...)
	if err != nil {
		grpclog.Fatalln(err)
	}
	defer conn.Close()

	// 初始化客户端
	c := service.NewHelloServiceClient(conn)

	// 构造请求
	req := &service.HelloRequest{Id: 1}
	// 调用服务
	resp, err := c.SayHello(context.Background(), req)

	// 服务调用失败
	if err != nil {
		grpclog.Fatalln(err)
	}

	fmt.Println("resp.Name = ", resp.Name)
}
