package main

import (
	"demo1-helloword/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
	"net"
)

// 具体server代码
func main() {

	// 创建新服务
	server := grpc.NewServer()
	// 注册服务
	service.RegisterHelloServiceServer(server, &service.HelloService{})

	// 设置端口监听服务
	listen, err := net.Listen("tcp", "127.0.0.1:9091")
	if err != nil {
		grpclog.Fatalf("Failed to listen: %v", err)
	}
	server.Serve(listen)
}
