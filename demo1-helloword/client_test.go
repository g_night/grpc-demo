package main

// 客户端示例

import (
	"context"
	"demo1-helloword/service"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/grpclog"
	"testing"
)

func TestClient(t *testing.T) {
	// 连接 127.0.0.1:9091，地址要一致
	conn, err := grpc.Dial("127.0.0.1:9091", grpc.WithInsecure())
	if err != nil {
		grpclog.Fatalln(err)
	}
	defer conn.Close()

	// 初始化客户端
	c := service.NewHelloServiceClient(conn)

	// 构造请求
	req := &service.HelloRequest{Id: 1}
	// 调用服务
	resp, err := c.SayHello(context.Background(), req)

	// 服务调用失败
	if err != nil {
		grpclog.Fatalln(err)
	}

	fmt.Println("resp.Name = ", resp.Name)
}
