package service

import (
	"context"
	"strconv"
)

// hello服务具体实现

type HelloService struct{}

// 方法名&参数  --> 就在 hello.pb.go 的 HelloServiceServer
func (server *HelloService) SayHello(ctx context.Context, req *HelloRequest) (*HelloResponse, error) {
	number := strconv.Itoa(int(req.Id))
	resp := &HelloResponse{Name: "hello word no." + number}
	return resp, nil
}
